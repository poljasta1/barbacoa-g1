﻿== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Ayuso Martínez, Álvaro
* Chura Pascual, Albaro
* Cote Llamas, José
* Herreria Oña, Asier
* Iglesias Trujillo, Carmen
* Juan Chico, Jorge
* Juan Rodríguez, Simón
* Moreno Monrobe, Alberto
* Pérez Peña, Antonio
* Triguero Navarro, Antonio

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Moto de Jorge (2 plazas)

* Ayuso Martínez, Álvaro
* Jorge Juan-Chico

==== Coche de Alberto (4 plazas)

* Chura Pascual, Albaro
* Juan Rodríguez, Simón
* Moreno Monrobe, Alberto

==== Ala delta de Asier (1 plazas)

* Herreria Oña, Asier

==== Coche de Antonio (4 plazas)

* Pérez Peña, Antonio
* Triguero Navarro, Antonio

==== Chopper de mekingeniero de José (2 plazas)

* Cote Llamas, José
* Iglesias Trujillo, Carmen
